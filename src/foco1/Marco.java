package foco1;

import java.awt.Graphics;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import eventos.RecolectorDeSistema;

public class Marco extends JFrame{
	public Marco() {
		this.setVisible(true);
		this.setBounds(200,200,(int)RecolectorDeSistema.getWidth()/2 ,(int)RecolectorDeSistema.getHeight()/2);
		Panel panel=new Panel();
		this.add(panel);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}

class Panel extends JPanel{
	JTextField cuadro1;
	JTextField cuadro2;
	public void paintComponent(Graphics lamina) {
		super.paintComponent(lamina);
		setLayout(null);
		cuadro1=new JTextField();
		cuadro2=new JTextField();
		cuadro1.setBounds(120,10,150,20);
		add(cuadro1);
		cuadro2.setBounds(120,50,150,20);
		add(cuadro2);
		LanzaFocos elFoco=new LanzaFocos();
		cuadro1.addFocusListener(elFoco);
	}
	
	private class LanzaFocos implements FocusListener{

		@Override
		public void focusGained(FocusEvent e) {
			// TODO Auto-generated method stub
			
			
		}

		@Override
		public void focusLost(FocusEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
}


//construir cuadros de textos, clase JTextField