package cursoPildorasInformaticas;
import java.util.*;

public class UsoTallas {
	
	//creamos enum
	//enum Talla {MINI, MEDIANO, GRANDE, MUYGRANDE};
	
	enum Talla{
		
		MINI("S"), MEDIANO("M"), GRANDE("L"), MUYGRANDE("XL");
		
		private Talla(String abreviatura) {
		this.abreviatura=abreviatura;
		
	}
	public String getAbreviatura() {
		return abreviatura;
	}
	private String abreviatura;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner entrada = new Scanner(System.in);
		System.out.println("Escribe talla: MINI/MEDIANO/GRANDE/MUYGRANDE");
		String entradaDatos = entrada.next().toUpperCase();
		Talla laTalla = Enum.valueOf(Talla.class, entradaDatos);
		
		System.out.println("TALLA:= " + laTalla);
		System.out.println("Abreviatura= " + laTalla.getAbreviatura());

	}

}

