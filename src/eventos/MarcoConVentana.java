package eventos;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

public class MarcoConVentana extends JFrame{
	public MarcoConVentana() {
		
	this.setVisible(true);
	setTitle("Respondiendo");
	setBounds(300,300,500,350);
	
	M_Ventana oyenteVentana=new M_Ventana();
	addWindowListener(oyenteVentana);
	}
}

class M_Ventana implements WindowListener{
	
	public void windowActivated(WindowEvent e) {
		System.out.println("Ventana activada");
	}
	public void windowClosed(WindowEvent e) {
		Object ventana=e.getSource().toString();
		System.out.println("Ventana" + ventana);
		
	}

	public void windowClosing(WindowEvent e) {
	}

	public void windowDeactivated(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {
	}

	public void windowIconified(WindowEvent e) {
		System.out.println("ventana minimizada");
	}

	public void windowOpened(WindowEvent e) {
		Object ventana=e.getSource().toString();
		System.out.println("Ventana abierta");
	}

	
	
	
}