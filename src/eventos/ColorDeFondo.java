package eventos;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

class MiComponente extends JPanel {
	//public void paintComponent(Graphics g) {
	//	super.paintComponent(g);
		
	//}
	boolean background = true;
	
	JButton botonBlack=new JButton("Black");
	JButton botonRojo=new JButton("Rojo");
	JButton botonAmarillo=new JButton("Amarillo");
	public MiComponente() {
		//int height=(int)RecolectorDeSistema.getHeight()/8;
		//int width=(int) RecolectorDeSistema.getWidth()/8;
		add(botonBlack);
		add(botonRojo);
		add(botonAmarillo);
		ColorDeFondo black=new ColorDeFondo(Color.black);
		ColorDeFondo rojo=new ColorDeFondo(Color.red);
		ColorDeFondo amarillo=new ColorDeFondo(Color.yellow);
		botonBlack.addActionListener(black); 
		botonRojo.addActionListener(rojo);
		botonAmarillo.addActionListener(amarillo);
											// addAction objeto evento
	}										//objeto fuente-> botonAzul
											//objeto listener-> ActionEvent evento
	
private class ColorDeFondo implements ActionListener{
	private Color colorDeFondo;
	public ColorDeFondo(Color c) {
		colorDeFondo=c;
	}
	public void actionPerformed(ActionEvent e) {
		setBackground(colorDeFondo);
	}
}
}
