package eventos;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class VentanaConWindowsAdapter extends JFrame {
	int height=(int)RecolectorDeSistema.getHeight();
	int width=(int)RecolectorDeSistema.getWidth();	
	
	public VentanaConWindowsAdapter() {
		this.setVisible(true);
		this.setBounds(200, 250, width/2, height/2);
		this.setTitle("Ventana con clase windowsAdapter");
		Lamina lamina=new Lamina();
		this.add(lamina);
		LaminaWindowAdapter listener=new LaminaWindowAdapter();
		this.addWindowListener(listener);
	}
	
	public int getHeight() {
		return height;
	}
	public int getWidth() {
		return width;
	}
	
}

//creamos la lamina
class Lamina extends JPanel{
	public void paintComponent(Graphics grafismo) {
		//Graphics miGrafico=new Graphics();
		super.paintComponent(grafismo);
		//grafismo.drawString("Estamos aprendiendo swing", 200, 200);
	}

}

//este es el evento creado
class LaminaWindowAdapter extends WindowAdapter{
	public void windowClosed(WindowEvent e) {
		System.out.println("Se cierra la ventana");
	}
	public void windowOpened(WindowEvent e) {
		Object ventana=e.getSource().toString();
		System.out.println("Ventana abierta");
	}
}

