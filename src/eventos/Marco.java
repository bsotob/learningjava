package eventos;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

class Marco extends JFrame{
	private double monitorWidth;
	private double monitorHeight;
	public Marco(double monitorWidth, double monitorHeight) {
		
		this.setBounds(0, 0, (int)monitorHeight, (int)monitorWidth);
		this.setVisible(true);
		this.setTitle("Marco con lamina básico");
		MiComponente lamina=new MiComponente();
		addComponente(lamina);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	public void addComponente(MiComponente componente) {
		this.add(componente);
	}
	
}
