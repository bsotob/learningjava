package enventoMouse2;

import eventos.RecolectorDeSistema;

import javax.swing.JFrame;

public class Marco extends JFrame {
	public Marco() {
		
		this.setVisible(true);
		this.setBounds(200, 200,(int)RecolectorDeSistema.getWidth()/2,(int)RecolectorDeSistema.getHeight()/2);
		this.setTitle("Marco eventoMouse2");
		MouseEvento mouseListener=new MouseEvento();
		this.addMouseListener(mouseListener);
		this.addMouseMotionListener(mouseListener);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}
