package graficos;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Ellipse2D.Double;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;


public class TestDraw {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MarcoConDibujos miMarco=new MarcoConDibujos();
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

}
class MarcoConDibujos extends JFrame{
	public MarcoConDibujos() {
		this.setTitle("Prueba de dibujo");
		this.setSize(1900,1800);
		this.setLocation(350, 350);
		this.setVisible(true);
		LaminaConFiguras miLamina=new LaminaConFiguras();
		this.add(miLamina);
		miLamina.setBackground(SystemColor.window);

		
	}
}
class LaminaConFiguras extends JPanel {
	
	public void paintComponent(Graphics grafismo) {
		super.paintComponent(grafismo);
		//ahora vamos a dibujar
		//dentro de la clase grafics
		//grafismo.drawRect(100, 50, 200, 200);
		//grafismo.drawArc(50, 100, 200, 120, 120, 150);
		//Graphics2d g2=(Graphics2d) grafismo;
		Graphics2D g2=(Graphics2D) grafismo;
		
		Rectangle2D rectangulo=new Rectangle2D.Double(100,100,200,150);
		g2.setPaint(Color.DARK_GRAY);
		g2.fill(rectangulo);
		g2.setPaint(Color.GREEN);
		g2.draw(rectangulo);
		Ellipse2D elipse=new Ellipse2D.Double();
		elipse.setFrame(rectangulo);
		g2.setPaint(Color.blue);
		g2.draw(elipse);
		g2.setPaint(Color.CYAN);
		g2.draw(new Line2D.Double(100, 100, 300, 250));
		g2.setPaint(Color.magenta);
		g2.fill(elipse);
		//g2.setPaint(Color.RED);
		//g2.fill(rectangulo);
		//g2.setPaint(Color.BLUE);
		//double centroEnX=rectangulo.getCenterX();
		//double centroEnY=rectangulo.getCenterY();
		//double radio=150;
		//Ellipse2D circulo=new Ellipse2D.Double();
		//circulo.setFrameFromCenter(centroEnX, centroEnY, centroEnX+radio, centroEnY+radio);
		//g2.draw(circulo);
		//g2.setPaint(Color.RED);
		
		
		

		
	}
}
