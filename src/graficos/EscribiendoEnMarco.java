package graficos;
import javax.swing.*;
import java.awt.*;

public class EscribiendoEnMarco {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MarcoConTexto miMarco=new MarcoConTexto();
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}

}

class MarcoConTexto extends JFrame{
	public MarcoConTexto() {
		this.setVisible(true);
		this.setSize(600, 450);
		this.setLocation(400, 200);
		this.setTitle("Primer texto");
		Lamina miLamina=new Lamina();
		this.add(miLamina);
	}
}
class Lamina extends JPanel {
	public void paintComponent(Graphics grafismo) {
		//Graphics miGrafico=new Graphics();
		super.paintComponent(grafismo);
		grafismo.drawString("Estamos aprendiendo swing", 200, 200);
		
	}
}
