import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.*;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class TestImagenes {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MarcoConTexto miMarco=new MarcoConTexto();
		miMarco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}

}

class MarcoConTexto extends JFrame{
	//Image imagen=ImageIO.read(File("solo.png"));
	
	public MarcoConTexto() {
		this.setVisible(true);
		this.setSize(2000, 2000);
		this.setLocation(400, 200);
		this.setTitle("Primer texto");
		Lamina miLamina=new Lamina();
		this.add(miLamina);
	}
}
class Lamina extends JPanel {
	private Image imagen;
	public void paintComponent(Graphics grafismo) {
		//Graphics miGrafico=new Graphics();
		super.paintComponent(grafismo);
		File pathImage=new File("vader.png");
		
		//necesitamos capturar una excepcion por el metodo imageIO
		try {
		imagen=ImageIO.read(pathImage);
		}
		catch(IOException e) {
			System.out.println("No se encuentra la imagen, revise la ruta");
		}
		grafismo.drawImage(imagen, 15, 15, null);
		int ancho=imagen.getWidth(this);
		int altura=imagen.getHeight(this);
		//grafismo.copyArea(0, 0, 100, 100, 300, 200);
		for(int i=0; i<100;i++) {
			//grafismo.copyArea(i, i, 100 +i, 100+i, 300 + i, 200 + i);
			
			for(int j=0; j<200;j++) {
				grafismo.copyArea(0, 0, ancho*i, altura*i, ancho*i, altura*j);;
			}
		}
		grafismo.drawString("Q U E  P A S A  P A B L E R A S", 200, 200);
		
	}
}