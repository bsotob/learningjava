package eventos3;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

import eventos.RecolectorDeSistema;

public class MarcoEstado extends JFrame{
	public MarcoEstado() {
		this.setVisible(true);
		RecolectorDeSistema monitor=new RecolectorDeSistema();
		this.setBounds(200, 200, (int)monitor.getWidth()/2, (int)monitor.getHeight()/2);
		CambioEstado listener=new CambioEstado();
		this.addWindowStateListener(listener);
	}
	

}

