package poo;

public class Coche {
	
	private int ruedas;
	private int largo;
	private int ancho;
	private int motor;
	private int peso;
	
	public Coche() {
		ruedas=4;
		largo=2000;
		ancho=300;
		motor=1600;
		peso=500;
	}
	
	public int getRuedas() {
		return this.ruedas;
	}
	public int getLargo() {
		return largo;
	}
	public int getAncho() {
		return ancho;
	}
	public int getMotor() {
		return motor;
	}
	public int getPeso() {
		return peso;
	}
	public void setPeso(int peso) {
		this.peso = peso;
		
	}
	

}
