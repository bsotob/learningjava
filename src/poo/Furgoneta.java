package poo;

public class Furgoneta extends Coche{
	private int plazasExtras;
	private int capacidadExtra;
	
	public Furgoneta(int peso, int plazasExtras, int capacidadExtra) {
		super(); //llamamos al constructor de la clase
		this.setPeso(peso);
		this.plazasExtras=plazasExtras;
		this.capacidadExtra=capacidadExtra;
	}

}
