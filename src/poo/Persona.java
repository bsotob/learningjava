package poo;

		abstract class Persona {
	private String nombre;
	private String descripcion;
	public Persona(String nombre) {
		this.nombre=nombre;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	public abstract String getDescription();

}
