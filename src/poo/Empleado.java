package poo;

import java.util.Calendar;

class Empleado implements Comparable, Trabajadores{
	private String nombreEmpleado;
	private double sueldo;
	private int anyo;
	private int mes;
	private int dia;
	private Calendar altaContrato;
	private String decision;
	
	public Empleado(double sueldo, String nombre, int anyo, int mes, int dia){
		this.sueldo=sueldo;
		this.nombreEmpleado=nombre;
		this.anyo=anyo;
		this.mes=mes;
		this.dia=dia;
		altaContrato = Calendar.getInstance();
		
	}
	
	
	public String getInfo() {
		
		String info= ("info empleado: \nNombre: " + this.nombreEmpleado + "\n"
				+ "sueldo: " + this.sueldo + " euros/mensuales \n"
				+ "Fecha: " + this.anyo + "/" + this.mes + "/" + this.dia + "\n"
						+ "altaContrato : " + altaContrato.getTime());
		return info;
		
	}
	public void setSueldo(double subidaSueldo) {
		this.sueldo=this.sueldo + subidaSueldo;
	}
	
	public String getName() {
		return this.nombreEmpleado;
	}
	public double getSueldo() {
		return this.sueldo;
	}
	public Calendar getAltaContrato() {
		return this.altaContrato;
	}
	public int compareTo(Object miObject) {
		Empleado objectComparable=(Empleado) miObject;
		if(this.getSueldo()>objectComparable.getSueldo()) {
			return -1;
		}
		if(this.getSueldo()<objectComparable.getSueldo()) {
			return 1;
		}
		return 0;
	}
	public double setBonus(double gratificacion) {
		return Trabajadores.bonusBase+gratificacion;
	}
	
}