package poo;


public class Jefes extends Empleado implements Jefatura{
	private int numDespacho;
	private int bonus;
	private int subidaSueldo;
	private String decision;
	public Jefes(double sueldo,String nombre, int numDespacho, int bonus,int anyo, int mes, int dia) {
		super(sueldo,nombre,anyo,mes,dia);
		this.numDespacho=numDespacho;
		this.bonus=bonus;
		
	}
	public Jefes(double sueldo,String nombre, int numDespacho, int bonus, int subidaSueldo,int anyo, int mes, int dia) {
		super(sueldo,nombre,anyo,mes,dia);
		this.numDespacho=numDespacho;
		this.bonus=bonus;
	}
	
	public void setSubidaSueldo() {
		super.setSueldo(this.bonus);
	}
	
	public String setDecision(String decision) {
		this.decision=decision;
		return("decisión: " + this.decision);
	}
	
	public double setBonus(double gratificacion) {
		double prima=2000;
		return Trabajadores.bonusBase+gratificacion+prima;
	}
	

}
